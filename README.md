# polymerapp

Polymerjs 3.0 starter app for Medium article

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development purposes.

### Prerequisites

What things you need to install the software and how to install them

 - [Git](https://git-scm.com/downloads)
 - [Nodejs](https://nodejs.org/en/download/)
 - [npm](https://www.npmjs.com/)

### Installation

```
npm install -g polymer-cli@next
git clone https://westdabestdb@bitbucket.org/westdabestdb/polymerapp.git
```
### Development Setup & Serve

```
cd polymerapp
npm install
polymer serve
your application is ready on http://127.0.0.1:8081/
```

## Authors

* **Gorkem Erol** - *westdabestdb* - [My Website](http://westdabestdb.io/)